import java.util.*;
import java.io.*;

public class TimeShare
{
	//golbal variables for the CPU
	private static int clock = 1;
	private static int idle = 0;
	private static int usage = -1;

	public static void main (String [] args)
	{	
		//Create a Queue that will store the data from the 
		//input file
		Queue inputQ = new Queue();
		jobInput(args, inputQ);
		print();
		
		//Send the Queue (now with Job data) into the 
		//control method that will perform the rest
		//of the programs task
		control(inputQ);	
	}

	//Checks the Queues if they are empty and goes through each
	//process if not. There are three queues, input, pricess, completion
	private static void control(Queue input)
	{
		Queue job = new Queue();
		Queue finish = new Queue();
		
		while((!input.isEmpty()) || (!job.isEmpty()))
		{
			//If arrival time is the same as the clock time
			//move item from input queue to job queue
			if(!input.isEmpty())
			{
				Job temp = (Job) input.front();
				if(temp.arrivalTime == clock)
				{
					job.enqueue(input.front());
					input.dequeue();
				}
			}
			//If job has completed, move it from the job queue 
			//to the finish queue. Update the turnaround time
			if(!job.isEmpty())
			{
				Job temp = (Job) job.front();
				int rT = temp.runTime;
				int sT = temp.startTime;
				if((rT == (clock - sT)) && (sT != 0))
				{
					temp.turnTime = temp.waitTime + rT;
					finish.enqueue(temp);
					job.dequeue();
				}
			}
			//If job hasnt been started, and arrival time is smaller
			//or equal to the clock time, start job
			//Update the start time and wait time for job
			if(!job.isEmpty())
			{
				Job temp = (Job) job.front();
				int aT = temp.arrivalTime;
				int sT = temp.startTime;
				if((aT <= clock) && (sT == -1))
				{
					temp.startTime = clock;
					temp.waitTime = clock - aT;
					
					changeFront(job, clock, (clock - aT));
				}
			}
			//Update the CPU counters
			if((job.isEmpty()) && (!input.isEmpty()))
				idle++;
			else
				usage++;
			clock++;
		}
		//Calculate the average wait time and print the summary report
		double avgWT = results(finish);
		printCPU(avgWT);
		
	}
	
	//Updates the the first item in each Queue with the current
	//start time wait time	
	private static void changeFront(Queue q, int sT, int wT)
	{
		Queue temp = new Queue();
		
		int num = 0;
		while(!q.isEmpty())
		{
			if(num == 0)
			{
				Job a = (Job) q.front();
				a.startTime = sT;
				a.waitTime = wT;
				temp.enqueue(a);
			}
			else
				temp.enqueue(q.front());
			q.dequeue();
		}
		while(!temp.isEmpty())
		{
			q.enqueue(temp.front());
			temp.dequeue();
		}
	}
	
	//Methods prints all of the CPU information that is necessary
	//such as idle time and usage
	private static void printCPU(double avgW)
	{
		
		int length;
		String waitTime = "Average Wait Time";
		String cUsage = "CPU Usage";
		String cIdle = "CPU Idle";
		String cPercent = "CPU Usage (%)";
		System.out.println();
		for(length = 25; length > waitTime.length(); length--)
			System.out.print(" ");
		System.out.println(waitTime + " => " + decFormat(avgW));
		for(length = 25; length > cUsage.length(); length--)
			System.out.print(" ");
		System.out.println(cUsage + " => " + decFormat((double)usage));
		for(length = 25; length > cIdle.length(); length--)
			System.out.print(" ");
		System.out.println(cIdle + " => " + decFormat(idle));
		double percU = 100 * ( (double)usage / (usage + idle) );
		for(length = 25; length > cPercent.length(); length --)
			System.out.print(" ");
		System.out.println(cPercent + " => " + decFormat(percU) + "%");
		System.out.println();
	}

	//Formats all doules to have two decimal places
	private static String decFormat(double numb)
	{
		String formatter = String.format("%.2f", numb);
		return formatter;
	}
	
	//Takes job data from input file and adds it to the input Queue
	private static void jobInput(String [] arg, Queue q)
	{
		String fileName;
		ArrayList<Job> jobs = new ArrayList<Job>();
		if(arg.length == 0)
			fileName = "";
		else
			fileName = arg[0];
		int num = 0;
		try{
			Scanner scan = new Scanner(new File(fileName));
			while(scan.hasNext())
			{
				//System.out.println("TEST");
				Scanner in = new Scanner(scan.nextLine());

				String jobID = in.next();
				int tArrive = in.nextInt();
				int tRun = in.nextInt();
				
				//Add the Jobs to an arraylist to make the sort 
				//by arrival time easier
				jobs.add(new Job(jobID, tArrive, tRun));
				num++;
				
			}
		}
		catch(IOException e) {
			System.out.println(e.getMessage()); }
		//Sort the jobs then add them to the Queue
		sortJobs(jobs);
		for(int i = 0; i < jobs.size(); i++)
		{
			q.enqueue(jobs.get(i));
			//System.out.println(i);
		}
	}

	//This method uses a selection sort to sort all of the Jobs
	//by their arrival time. This is done in an arraylist for simplicity
	private static void sortJobs(ArrayList<Job> jobs)
	{
		int minIndex, index, j;
		Job temp;

		for(index = 0; index <jobs.size() - 1; index++)
		{
			minIndex = index;
			for(j = minIndex+1; j < jobs.size(); j++)
			{
				if(jobs.get(j).arrivalTime < jobs.get(minIndex).arrivalTime)
					minIndex = j;
			}
			if(minIndex != index)
			{
				temp = jobs.get(index);
				jobs.set(index, jobs.get(minIndex));
				jobs.set(minIndex, temp);
			}
		}
	}
	
	//Print basic header information for the report
	private static void print()
	{
		System.out.println("\n" + "Job Control Analysis : Summary Report");
		System.out.println("-------------------------------------");
		System.out.println("job id  arivval   start    run  wait   turnaround");
		System.out.println("        time      time    time  time   time");
		System.out.println("------  -------   -----   ----  ----   ----------");
	}
	
	//Prints the individual results of each item in the queue
	//and also calculates the average wait time
	//Returns the average wait time
	private static double results(Queue q)
	{
		int loops = 0;
		int wTTotal = 0;
		double wTAvg;
		while(!q.isEmpty())
		{
			//System.out.println("Testing..");
			Job curr = (Job) q.front();
			String id = curr.jobName;
			int aT = curr.arrivalTime;
			int sT = curr.startTime;
			int rT = curr.runTime;
			int wT = curr.waitTime;
			int tT = curr.turnTime;

			System.out.print(id + "       ");
			if(aT < 10)
				System.out.print(" ");
			System.out.print(aT + "       ");
			if(sT < 10)
				System.out.print(" ");
			System.out.print(sT + "    ");
			if(rT < 10)
				System.out.print(" ");
			System.out.print(rT + "     ");
			if(wT < 10)
				System.out.print(" ");
			System.out.print(wT + "       ");
			if(tT < 10)
				System.out.print(" ");
			System.out.print(tT + "\n");
			
			wTTotal += wT;
			loops++;
		
			q.dequeue();
		}
		wTAvg = (double) wTTotal / loops;

		return wTAvg;
	}
}//End of TimeShare
