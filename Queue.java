
public class Queue implements QueueInterface {
  private Node lastNode;
  
  public Queue() {
    lastNode = null;   
  }  // end default constructor
  
  // queue operations:
  public boolean isEmpty() {
        return (lastNode == null);
  }  // end isEmpty

  public void dequeueAll() {
  	lastNode = null;
  }  // end dequeueAll

  public void enqueue(Object newItem) {
    Node temp = new Node(newItem);
    // insert the new node
    if (isEmpty()) {
      // insertion into empty queue
      temp.setNext(lastNode);
      lastNode = temp;
      lastNode.setNext(lastNode);
    }
    else {
      // insertion into nonempty queue
      Node firstNode = lastNode.getNext();
      temp.setNext(firstNode);
      lastNode.setNext(temp);
    }  // end if
    lastNode = temp;
  }  // end enqueue

  public Object dequeue() throws QueueException {
    if (!isEmpty()) {
      // queue is not empty; remove front
	Object temp = lastNode.getNext().getItem();
	Node firstNode = lastNode.getNext();
	
	if(lastNode == firstNode)
		lastNode = null;
	else
	{
		lastNode.setNext(firstNode.getNext());
	}
	return temp;
    }  //end if
    else {
      throw new QueueException("QueueException on dequeue:"
                             + "queue empty");
    }  // end if
  }  // end dequeue

  public Object front() throws QueueException {
    if (!isEmpty()) {
      Node firstNode = lastNode.getNext();/*
      if(firstNode == null)
	return lastNode.getItem();
      else
      	return firstNode.getItem();*/
      return firstNode.getItem();
    }
    else {
      throw new QueueException("QueueException on front:"
                             + "queue empty");
    }
  }

  public Object clone() throws CloneNotSupportedException
  {
	boolean copied = false;
        Queue copy = new Queue();
        Node curr = lastNode, prev = null;
        while ( (! copied) && (lastNode != null) )
        {
                Node temp = new Node(curr.getItem());
                if (prev == null)
                        copy.lastNode = temp;
                else
                        prev.setNext(temp);
                prev = temp;
                curr = curr.getNext();
		copied = (lastNode == curr);
        }
	prev.setNext(copy.lastNode);
        return copy;
  }
} // end Queue
