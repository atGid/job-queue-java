public class QueTester
{
	public static void main (String [] args)
	{	try{
		//Test the Queue class
		Queue a = new Queue();
		a.enqueue("A");
		a.enqueue("B");
		a.enqueue("C");
		printQ(a);
		System.out.println("Front = " + a.front());
		System.out.println("Item dequeued " + a.dequeue());
		//a.dequeue();
		System.out.println("New item in front = " + a.front());
		a.enqueue("D");
		a.enqueue("E");
		a.enqueue("F");
		System.out.println("Front = " + a.front());
		System.out.println("Item 1 dequeued " + a.dequeue() 
			+ "\n" + "Item 2 dequeued " + a.dequeue());
		System.out.println("New front = " + a.front());
		printQ(a);
		System.out.println("Dequue all...");
		a.dequeueAll();
		System.out.println("Empty Queue? " + a.isEmpty());
		printQ(a);
		a.enqueue("New Object");
		printQ(a);
		a.dequeue();
		printQ(a);		

		//Testing the Job class
		Job b = new Job("This is a job", 117, 117);
		System.out.println(b.runTime);
		}
		catch(CloneNotSupportedException e) {}
	} 
	public static void printQ(Queue q) throws CloneNotSupportedException
	{
		System.out.println("Printing Queue...");
		Queue temp = new Queue();
		if(!q.isEmpty())
			temp = (Queue) q.clone();
		if(temp.isEmpty())
			System.out.println("Queue is Empty");
		for(int i =0; i < 5; i++)
		{
			if(!temp.isEmpty()) {
				System.out.print(temp.front() + " ");
				temp.dequeue();
			}
		}
		System.out.println();
	}
}
