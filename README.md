JobID	Arrive	Start	Run	Wait	Turnaround
-----	------	-----	---	----	----------
Job1	1	1	2	0	2
Job2	2	3	5	1	6
Job3	5	8	1	3	4
Job3	8	9	3	1	4
Job5	9	10	3	1	4

Avg Wait: 1.2
CPU Usage: 14
CPU Idle: 0
CPU Usage (%): 100%

Begin assignment by working on the Queue class
The following methods need to be updated and tested
	- isEmpty
	- dequeueAll
	- enqueue
	- dequeue
	- front
After the methods are created create a Sample test program to test
the Queue method. It is important that the queue runs correctly for the rest
of the program to flow successfully.
Make sure that the circular arraylist is truly working as a QUEUE
and not as a stack. Make sure the dequeue comes from the front of
the queue. Make sure you can still dequeue an item if there is only one in
the circular array

The program will read a sequeuence on job blocks and store them over time when they are 
to be processed. 
The main portion of the program will come down to the controll logic that is shown to us in the
assignment packet.
Be careful to cast variables correctly, use the global variables correctly, move jobs between
each queue effectively.

Create one or two sample jobs (including the one provided) to make sure program runs correctly.

Other parts to add to the program include:
-A printer for the heading and other informaton
-A method to input job data from file to the program (use args)
